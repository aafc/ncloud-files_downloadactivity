OC.L10N.register(
    "files_downloadactivity",
    {
    "Downloaded by {actor} (via desktop)" : "Завантажено {actor} (via desktop) ",
    "Downloaded by {actor} (via app)" : "Завантажено {actor} (via app) ",
    "Downloaded by {actor} (via browser)" : "Завантажено {actor} (через бравзер)",
    "Shared file {file} was downloaded by {actor} via the desktop client" : "Файл у спільному доступі {file} було завантажено {actor} через настільний клієнт ",
    "Shared file {file} was downloaded by {actor} via the mobile app" : "Файл у спільному доступі {file} було завантажено {actor} через мобільний застосунок ",
    "Shared file {file} was downloaded by {actor} via the browser" : "Спільний файл {file} було завантажено {actor} через бравзер",
    "A local shared file or folder was <strong>downloaded</strong>" : "Файл або каталог на пристрої, що у спільному доступі, було  <strong>завантажено</strong>",
    "Activities for shared file downloads" : "Дії для завантажених спільних файлів",
    "Creates activities for downloads of files that were shared with other users or a group" : "Створює дії для завантаження файлів, до яких було надано спільний доступ іншим користувачам або групі"
},
"nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);");
